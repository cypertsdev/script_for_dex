###########################################
#For MOON cold Wallet
###########################################

#!/bin/bash

useradd=$1
adminaccount="0x63a04a0736E9Ea250d4Fa9C7Ef95f31d15ee45B0"
password=$2

echo  "user Address =$useradd     Password=$password"

var1=$(curl -X GET "http://87.121.98.59:4000/api?module=account&action=balance&address=$useradd" --silent -H "accept: application/json")
str1=$(echo $var1 | jq ".result" | bc)
satoshi=1000000000000000000
balanceB=$(echo "scale=7;$str1 / $satoshi" | bc | awk '{printf "%f", $0}')
echo $balanceB

contract="0x2e7aff031fe994fdd9f0b8f2f15d97b1c1b7562f"
var2=$(curl -X GET "http://87.121.98.59:4000/api?module=account&action=tokenbalance&contractaddress=$contract&address=$useradd" --silent -H "accept: application/json")
str2=$(echo $var2 | jq ".result" | bc)
satoshi=1000000000000000000
balanceM=$(echo "scale=7;$str2 / $satoshi" | bc | awk '{printf "%f", $0}')
echo $balanceM

min=1

if [ 1 -eq "$(echo "${balanceB} < ${min}" | bc)" ];then
./BDT_Withdraw.sh $useradd 10400000 >> Moon_Fee_Fund.txt
echo "$min Given to $useradd" >> Moon_Fee_Fund.txt
fi
sleep 60
echo "going ahead..."

geth --datadir=~/geth-linux-amd64-1.9.0-52f24617/node --ipcpath=~/geth-linux-amd64-1.9.0-52f24617/node/geth.ipc attach << EOF | grep "Result :"
        var mooncoin=JSON.parse('[{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"tokenOwner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Approval","type":"event"}]');
        var moonContract=web3.eth.contract(mooncoin).at('0x2e7aff031fe994fdd9f0b8f2f15d97b1c1b7562f')
	personal.unlockAccount("$useradd", $password, 300);
	result=moonContract.transfer("$adminaccount",web3.toWei("$balanceM","ether"),{from:"$useradd"});
	console.log("Result :"+result);
EOF
