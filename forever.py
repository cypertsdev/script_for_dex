#!/usr/bin/python3.6
from subprocess import Popen
import sys

filename = sys.argv[1]
while True:
    print("\nStarting " + filename)
    p = Popen("python3.6 " + filename, shell=True)
    p.wait()
