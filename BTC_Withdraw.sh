#!/bin/sh

wallet_balance=$(bitcoin-cli getbalance)

#vari=$(curl -X POST https://www.moonmex.io/UserApi/getbalances -d userName=admin | jq -r '.data.BTCbalance')
withdraw_address=$1

withdraw_balance=$2
satoshi=100000000
payout=$(echo "scale=8;$withdraw_balance / $satoshi" | bc | awk '{printf "%f", $0}')
#echo "payout $payout"
vars=$(echo "$wallet_balance -$withdraw_balance" | bc | awk '{printf "%f", $0}')

gateway_fee=$(echo "$payout -0.0005" | bc | awk '{printf "%f", $0}')

#echo "$withdraw_balance BTC Requested to withdraw"

#echo "$wallet_balance BTC available in wallet"

#echo "$gateway_fee Actual Coin to sent user after fee deduction"

#echo "$withdraw_address submited by User"

vari=0

varsl=$(echo "$vars > 0" | bc -l)
#echo "$varsl"
if [ $varsl -eq 1 ]; then
#echo "Enough Balance to approve request"
bitcoin-cli sendtoaddress "$withdraw_address" $(echo "$gateway_fee" | bc | awk '{printf "%f", $0}') 
else
echo "Balance is Low for withdraw Request"
fi
