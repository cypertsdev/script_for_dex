###########################################
#For BDT cold Wallet
###########################################

#!/bin/bash

#!/bin/bash

useradd=$1
adminaccount="0x63a04a0736E9Ea250d4Fa9C7Ef95f31d15ee45B0"
password=$2

geth --datadir=~/geth-linux-amd64-1.9.0-52f24617/node --ipcpath=~/geth-linux-amd64-1.9.0-52f24617/node/geth.ipc attach << EOF  | grep "Result :"
        personal.unlockAccount("$useradd", $password, 300);
        balance=web3.fromWei(eth.getBalance("$useradd"), "ether"); 
        tocold=balance-0.005;
        result=eth.sendTransaction({from:"$useradd",to:"$adminaccount", value:web3.toWei(tocold, "ether")});
	console.log("Result :"+result);
EOF
