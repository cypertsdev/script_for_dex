const web3 = require('web3')
const exec = require('child_process').exec

web3js = new web3()

function refreshProvider(web3Obj, providerUrl) {
    let retries = 0

    function retry(event) {
        if (event) {
            retries += 1
    
            if (retries > 5) {
            return setTimeout(refreshProvider, 5000)
            }
        } else {
            refreshProvider(web3Obj, providerUrl)
            subscribeBdt()
        }
  
        return null
    }

    const provider = new web3.providers.WebsocketProvider(providerUrl)
  
    provider.on('end', () => retry())
    provider.on('error', () => retry())

    web3Obj.setProvider(provider)

    return provider
}

refreshProvider(web3js, 'ws://0.0.0.0:8546')
subscribeBdt()

function subscribeBdt() {
    var subscription = web3js.eth.subscribe('pendingTransactions', function(error, result){
        if (error)
            console.log(error);
    })
    .on("data", function(txhash){
        console.log(txhash);
        
        web3js.eth.getTransaction(txhash).then(function(transaction) {
            console.log(transaction)
            const testscript = exec('echo ' + transaction.to + ' ' + transaction.value + ' ' + txhash )
        });
    });
}
