######################################
#For Send BTC to Cold Wallet..........
######################################
#!/bin/bash

balance=$(bitcoin-cli getbalance)
echo $balance
payout=$(echo "scale=8;$balance -0.001" | bc | awk '{printf "%f", $0}')

varsl=$(echo "$payout > 0" | bc -l)
echo "$payout"
echo "$varsl"

if [ $varsl -eq 1 ]; then
echo "good"
bitcoin-cli sendtoaddress "3CFirTQcMTPvesS2zqyG8sj8jGBWS86Us6" $(echo "scale=8;$payout" | bc | awk '{printf "%f", $0}') >> BtcTransaction_cold.txt
else
echo "verygood"
fi

