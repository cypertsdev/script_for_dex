#!/usr/bin/expect

set pass [lindex $argv 0]
log_user 0

spawn ./base.sh

expect "Your new account is locked with a password. Please give a password. Do not forget this password."

log_user 0
send "$pass\r"

log_user 0

expect "Passphrase:"

log_user 0

send "$pass\r"

expect "Repeat passphrase:"

log_user 0

log_user 1

expect "#";
