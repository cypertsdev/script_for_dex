#!/bin/bash

address="0x515e1C60198D2a463f26Da98725FBFD620df98b2"

var=$(curl -X GET "http://87.121.98.59:4000/api?module=account&action=balance&address=$address" --silent -H "accept: application/json")

str=$(echo $var | jq ".result" | bc)

satoshi=1000000000000000000
balance=$(echo "scale=7;$str / $satoshi" | bc | awk '{printf "%f", $0}')
echo $balance
