#!/bin/bash

var=$(bitcoin-cli gettransaction $1)
label=$(echo "$var" | jq '.details[0].label')
amount=$(echo "$var" | jq '.details[0].amount')
category=$(echo "$var" | jq '.details[0].category')
address=$(echo "$var" | jq '.details[0].address')
txid=$1
confirmations=$(echo "$var" | jq '.confirmations')
flag='"receive"'

min=0.002

if [ 1 -eq "$(echo "${amount} < ${min}" | bc)" ];then
   echo "Free $amount coins Added to hot wallet"
exit 1;
fi

echo $flag
if [ "$category" == "$flag" ];
then
if [ $confirmations -ge 1 ];
then

./Asset_transfer.sh $label $amount BTC

var=$(echo "$amount*100000000" | bc | awk '{printf "%d", $0}')

generate_post_data()
{
cat <<EOF
{
"username":$label,
"coinType":"BTC",
"amount":"$var"
}
EOF
}
curl -X POST https://bdex-gateway.herokuapp.com/user/deposit-coins -H 'cache-control: no-cache' -H 'content-type: application/json' --data "$(generate_post_data)"
fi
else

 echo "Send Transaction Failed"

fi
