from web3 import Web3
from web3.contract import ContractEvents
from web3.contract import ContractEvent

import sys
import datetime
import time
import sys
import json
import subprocess
import shlex

my_provider = Web3.WebsocketProvider('ws://127.0.0.1:8546')
w3 = Web3(my_provider)

def check_event(tx_hash, logs):
    for log in logs:
        for log_entry in log:
            if (log_entry['event'] == "Transfer"):
                sender = log_entry['args']['from']
                receiver = log_entry['args']['to']
                amount = log_entry['args']['tokens']
                subprocess.call(shlex.split('echo ' + receiver + ' ' + str(amount) + ' ' + tx_hash)) 

def handle_event(event_data, contract):
    tx_hash = event_data['transactionHash'].hex()
    while w3.eth.getTransactionReceipt(tx_hash) == None:
        time.sleep(5)
        print('Waiting on Tranasaction')

    receipt = w3.eth.getTransactionReceipt(tx_hash)
    events = [event['name'] for event in
              contract.events._events]
    logs = [contract.events.__dict__[event_name]().processReceipt(receipt) for event_name in events]

    check_event(tx_hash, logs)

def log_loop(event_filter, filterContract, poll_interval):
    print('Listening to transactions...')
    while True:  # keep logging indefinitely
        for event in event_filter.get_new_entries():
            handle_event(event, filterContract)
        time.sleep(poll_interval)

def main():
    address = '0xdac17f958d2ee523a2206206994597c13d831ec7'      #USDT

    abi_file = 'tt3.json'
    poll_interval = 5

    # load and parse abi file
    with open(abi_file) as f:
        abi_data = json.load(f)
        filterContract = w3.eth.contract(abi=abi_data, address=address)
        filter = w3.eth.filter({'fromBlock': 'latest', 'toBlock': 'latest', 'address': address})
        log_loop(filter, filterContract, poll_interval)


if __name__ == '__main__':
    main()
