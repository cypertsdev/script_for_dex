#!/bin/bash

address=$1
amount=$2
cointype="MOON"
transaction=$3
satoshi=1000000000000000000
if grep -qF "$transaction" moon_transactions.txt;
then
   echo "Transaction already exist"
exit 1;
else
   echo "$transaction">> moon_transactions.txt
fi

generate_post_data()
{
cat <<EOF
{
"address":"$address",
"coinType":"moon"
}
EOF
}

var=$(curl -X POST https://bdex-gateway.herokuapp.com/user/get-user-name -H 'content-type: application/json' -d "$(generate_post_data)")

str=$(echo $var | jq ".username")
echo $str
if [ "$str" == null ];then
   echo NULL
exit 1;
fi

amount1=$(echo "scale=7;$amount / $satoshi" | bc | awk '{printf "%f", $0}')

echo "./MOON_Send_Cold.sh $address $str" >> For_MOON_Cold.txt

min=10.0000 
if [ 1 -eq "$(echo "${amount1} < ${min}" | bc)" ];then
   echo "Free $amount coins Added to hot wallet"
exit 1;
fi


if [ $amount1 > 0 ];
then
./Asset_transfer.sh $str $amount1 MOON

var=$(echo "$amount1*10000000" | bc | awk '{printf "%d", $0}')

generate_post_data()
{
cat <<EOF
{
"username":$str,
"coinType":"MOON",
"amount":"$var"
}
EOF
}

curl -X POST https://bdex-gateway.herokuapp.com/user/deposit-coins -H 'cache-control: no-cache' -H 'content-type: application/json' --data "$(generate_post_data)"

else
echo "amount is less than 0"
fi

