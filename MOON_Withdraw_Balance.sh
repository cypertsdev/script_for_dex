#!/bin/bash

address="0x515e1C60198D2a463f26Da98725FBFD620df98b2"
contract="0x2e7aff031fe994fdd9f0b8f2f15d97b1c1b7562f"
var=$(curl -X GET "http://87.121.98.59:4000/api?module=account&action=tokenbalance&contractaddress=$contract&address=$address" --silent -H "accept: application/json")

str=$(echo $var | jq ".result" | bc)

satoshi=1000000000000000000
balance=$(echo "scale=7;$str / $satoshi" | bc | awk '{printf "%f", $0}')
echo $balance
