#!/bin/bash

address=$1
amt=$2
satoshi=10000000
var=$(echo "scale=7;$amt / $satoshi" | bc | awk '{printf "%f", $0}')
amount=$(echo "scale=7;$var -0.04" | bc | awk '{printf "%f", $0}')
HotWallet="0x515e1C60198D2a463f26Da98725FBFD620df98b2"
geth --datadir=~/geth-linux-amd64-1.9.0-52f24617/node --ipcpath=~/geth-linux-amd64-1.9.0-52f24617/node/geth.ipc attach  2>> BDT.log << EOF | grep "Result :"
 var balance="$amount";
 var fee=web3.fromWei(eth.estimateGas({from:"$HotWallet", to:"$address"}));
 var gasPrice=eth.gasPrice;
 personal.unlockAccount("$HotWallet", "admin", 300);
 var result=eth.sendTransaction({from:"$HotWallet",to:"$address", value:web3.toWei(balance, "ether")});
 console.log("Result :"+result);
EOF

